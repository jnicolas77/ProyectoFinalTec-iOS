//
//  RestauranteCell.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 4/26/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit

import AlamofireImage

class RestauranteCell: UITableViewCell {
    
    @IBOutlet weak var imgRestaurante: UIImageView!
    
    @IBOutlet weak var lblNombre: UILabel!
    
    @IBOutlet weak var lblDescripcion: UILabel!
    
    
    @IBOutlet weak var imgMoney1: UIImageView!
    
    @IBOutlet weak var imgMoney2: UIImageView!
    
    @IBOutlet weak var imgMoney3: UIImageView!
    
    @IBOutlet weak var imgMoney4: UIImageView!
    
    @IBOutlet weak var imgMoney5: UIImageView!
    
    
    @IBOutlet weak var imgStar1: UIImageView!
    
    @IBOutlet weak var imgStar2: UIImageView!
    
    @IBOutlet weak var imgStar3: UIImageView!
    
    @IBOutlet weak var imgStar4: UIImageView!
    
    @IBOutlet weak var imgStar5: UIImageView!
    
    var obj_restaurante:Restaurante!{
        didSet{
            self.updateUI();
        }
    };

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateUI(){
        
        imgRestaurante.af_setImage(withURL: obj_restaurante.image!);
        
        lblNombre.text = obj_restaurante.name!;
        
        lblDescripcion.text = obj_restaurante.slug;
        
        let starCount: Int = obj_restaurante.stars!;
        let priceCount: Int = obj_restaurante.price!;
        
        imgStar1.isHidden = true
        imgStar2.isHidden = true
        imgStar3.isHidden = true
        imgStar4.isHidden = true
        imgStar5.isHidden = true
        
        imgMoney1.isHidden = true
        imgMoney2.isHidden = true
        imgMoney3.isHidden = true
        imgMoney4.isHidden = true
        imgMoney5.isHidden = true
        
        for x in 1...starCount {
            switch x {
            case 1:
                imgStar1.isHidden = false
            case 2:
                imgStar2.isHidden = false
            case 3:
                imgStar3.isHidden = false
            case 4:
                imgStar4.isHidden = false
            case 5:
                imgStar5.isHidden = false
            default:
                imgStar5.isHidden = false
            }
        }
        
        for x in 1...priceCount {
            switch x {
            case 1:
                imgMoney1.isHidden = false
            case 2:
                imgMoney2.isHidden = false
            case 3:
                imgMoney3.isHidden = false
            case 4:
                imgMoney4.isHidden = false
            case 5:
                imgMoney5.isHidden = false
            default:
                imgMoney5.isHidden = false
            }
        }
        
        
        
        
    
    }
    
}
