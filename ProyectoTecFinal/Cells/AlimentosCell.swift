//
//  AlimentosCell.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 5/1/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit
import AlamofireImage

class AlimentosCell: UICollectionViewCell {
    
    @IBOutlet var picture: UIImageView?
    @IBOutlet var name: UILabel?
    @IBOutlet var price: UILabel?
    @IBOutlet var backView: UIView?
    
    
    var obj_alimento:Alimento!{
        didSet{
            self.updateUI();
        }
    };

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func updateUI(){
        
        name?.text = obj_alimento.name!;
        price?.text = "\(obj_alimento.price!)";
        picture?.af_setImage(withURL: obj_alimento.image!);
        //imgRestaurante.af_setImage(withURL: obj_restaurante.image!);
        
        
    }

    
}
