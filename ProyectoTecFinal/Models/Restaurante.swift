//
//  Restaurante.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 4/26/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class Restaurante: NSObject{
    
    var id_restaurante:String?;
    var name:String?;
    var slug:String?;
    var price:Int?;
    var stars:Int?;
    var image:URL?;
    
    
    typealias objDictionary = [String:AnyObject];
    
    
    override init() {
        // perform some initialization here
    }
    
    
    init(pDictionary:objDictionary) {
        
        self.id_restaurante = pDictionary["_id"] as? String;
        self.name = pDictionary["name"] as? String;
        self.slug = pDictionary["slug"] as? String;
        self.price = pDictionary["price"] as? Int;
        self.stars = pDictionary["stars"] as? Int;
        self.image = URL(string:pDictionary["image"] as! String);
        
        super.init()
        
    }
    
    
    static func parseData (_ jsonDic:[[String:AnyObject]])->[Restaurante] {
        var array_obj = [Restaurante]();
        do{
            
            for obj in jsonDic {
                
                let restaurante = Restaurante(pDictionary:obj)
                
                array_obj.append(restaurante);
            }
            
            
        }catch let error as NSError{
            print(error);
        }
        
        
        return array_obj;
        
    }

}
