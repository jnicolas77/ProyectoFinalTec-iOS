//
//  Alimento.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 5/1/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class Alimento: NSObject {

    var id_alimneto:String?;
    var name:String?;
    var information:String?;
    var price:Int?;
    var image:URL?;
    
    
    typealias objDictionary = [String:AnyObject];
    
    override init (){
        
        
    }
    
    
    init(pDictionary:objDictionary) {
        
        self.id_alimneto = pDictionary["_id"] as? String;
        self.name = pDictionary["name"] as? String;
        self.information = pDictionary["description"] as? String;
        self.price = pDictionary["price"] as? Int;
        self.image = URL(string:pDictionary["image"] as! String);
        
        super.init()
        
    }
    
    static func parseData (_ jsonDic:[[String:AnyObject]])->[Alimento] {
        var array_obj = [Alimento]();
        do{
            
            for obj in jsonDic {
                
                let alimento = Alimento(pDictionary:obj)
                
                array_obj.append(alimento);
            }
            
            
        }catch let error as NSError{
            print(error);
        }
        
        
        return array_obj;
        
    }

    
    
}

