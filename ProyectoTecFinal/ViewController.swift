//
//  ViewController.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 4/26/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NVActivityIndicatorViewable{
    
    
    var urlWS = "http://54.236.40.158:9000/restaurants";
    
    @IBOutlet weak var tvRestaurante: UITableView!
    
    var arrayRestaurante:[Restaurante]!;
    
    let size_loading = CGSize(width: 30, height:30);
    let type_loading = 23;
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil);
        let nib = UINib(nibName: "RestauranteCell", bundle: nil)
        tvRestaurante.register(nib, forCellReuseIdentifier: "RestauranteCell")
        
        
        self.tvRestaurante?.dataSource = self
        self.tvRestaurante?.delegate = self
        
        getRestaurantesList();
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayRestaurante == nil) ? 0 : arrayRestaurante!.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 183
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestauranteCell", for: indexPath) as! RestauranteCell
        
        let restaurante = arrayRestaurante! [indexPath.row];
        
        cell.obj_restaurante = restaurante;
        
        
        return cell
    }

    
    fileprivate func getRestaurantesList(){
        
        self.arrayRestaurante = [Restaurante]();
        
        startAnimating(size_loading, message: "", type: NVActivityIndicatorType(rawValue: type_loading))
        
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        print("url \(urlWS)");
        
        Alamofire.request(urlWS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                self.stopAnimating();
                if let data = response.result.value{
                    var arrayResultado = [[String:AnyObject]]()
                    let swiftyJsonResonse = JSON(data);
                    
                    if let resData = swiftyJsonResonse.arrayObject {
                        arrayResultado = resData as! [[String:AnyObject]]
                        self.arrayRestaurante = Restaurante.parseData(arrayResultado);
                    }
                    
                    self.tvRestaurante?.reloadData();
                    
                }else{
                    
                    self.tvRestaurante?.reloadData();
                    
                }
                break
                
            case .failure(_):
                self.stopAnimating();
                
                self.tvRestaurante?.reloadData();
                
                
                break
                
            }
        }
        
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "getAlimentosList", sender: self);
        
        print("Row \(indexPath.row) selected")
        
        tableView.deselectRow(at: indexPath, animated: true);
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //# MARK: - Listado Cotizaciones
        if segue.identifier == "getAlimentosList" {
            
            if let indexPath = tvRestaurante.indexPathForSelectedRow{
                
                let alimento = arrayRestaurante [indexPath.row];
                
                let controller = segue.destination as? AlimentosViewController
                controller?.obj_restaurant = alimento;
                    
                controller?.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller?.navigationItem.leftItemsSupplementBackButton = true
            }
            
            
            
        }
    }
    

}

