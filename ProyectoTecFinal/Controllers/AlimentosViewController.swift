//
//  AlimentosViewController.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 5/1/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON


class AlimentosViewController: UIViewController, NVActivityIndicatorViewable, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet var collectionViewAlimentos:UICollectionView?

    
    var urlWS = "http://54.236.40.158:9000/restaurants";
    
    var obj_restaurant:Restaurante!{
        didSet{
            urlWS += "/\(self.obj_restaurant.id_restaurante!)/foods"
        }
    }
    
    var arrayAlimento:[Alimento]!;
    
    var selectAlimento:Alimento?;
    
    let size_loading = CGSize(width: 30, height:30);
    let type_loading = 23;

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.title = obj_restaurant.name!;
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil);
        
        let nib = UINib(nibName: "AlimentosCell", bundle: nil)
        self.collectionViewAlimentos?.register(nib, forCellWithReuseIdentifier: "AlimentosCell")
        self.collectionViewAlimentos?.delegate = self;
        self.collectionViewAlimentos?.dataSource = self;
        
        
        self.getAlimentosList();
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (arrayAlimento == nil) ? 0 : arrayAlimento!.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlimentosCell", for: indexPath) as! AlimentosCell
        
        let alimento = arrayAlimento! [indexPath.row];
        
        cell.obj_alimento = alimento;
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 202)
    }

    fileprivate func getAlimentosList(){
        
        self.arrayAlimento = [Alimento]();
        
        startAnimating(size_loading, message: "", type: NVActivityIndicatorType(rawValue: type_loading))
        
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        print("url \(urlWS)");
        
        Alamofire.request(urlWS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                self.stopAnimating();
                if let data = response.result.value{
                    var arrayResultado = [[String:AnyObject]]()
                    let swiftyJsonResonse = JSON(data);
                    
                    if let resData = swiftyJsonResonse.arrayObject {
                        arrayResultado = resData as! [[String:AnyObject]]
                        self.arrayAlimento = Alimento.parseData(arrayResultado);
                    }
                    
                    self.collectionViewAlimentos?.reloadData();
                    
                }else{
                    
                    self.collectionViewAlimentos?.reloadData();
                    
                }
                break
                
            case .failure(_):
                self.stopAnimating();
                
                self.collectionViewAlimentos?.reloadData();
                
                
                break
                
            }
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectAlimento = self.arrayAlimento[indexPath.row];
        
        performSegue(withIdentifier: "getDetalleProducto", sender: self)
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //# MARK: - Listado Cotizaciones
        if segue.identifier == "getDetalleProducto" {
            
            let alimento = selectAlimento;
            
            let controller = segue.destination as? DetalleProductoViewController
            controller?.obj_producto = alimento;
            
            controller?.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            controller?.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
