//
//  DetalleProductoViewController.swift
//  ProyectoTecFinal
//
//  Created by Julio Nicolas on 5/3/17.
//  Copyright © 2017 Julio Nicolas. All rights reserved.
//

import UIKit
import AlamofireImage

class DetalleProductoViewController: UIViewController {
    
    @IBOutlet var imagenProducto:UIImageView?;
    @IBOutlet var nombreProducto: UILabel?;
    @IBOutlet var descriptionProducto: UILabel?;
    @IBOutlet var precioProducto: UILabel?;
    @IBOutlet var cantidadProducto: UITextField?
    
    
    var countProduct:Int = 1 {
        didSet{
        }
    };

    var obj_producto:Alimento!{
        didSet{
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = obj_producto.name!;
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil);
        
        
        // Do any additional setup after loading the view.
        
        imagenProducto?.af_setImage(withURL: obj_producto.image!);
        
        nombreProducto?.text = obj_producto.name!;
        descriptionProducto?.text = obj_producto.information!;
        precioProducto?.text = "\(obj_producto.price!)";
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    
    
    
    
    @IBAction func minButton(_ sender: Any) {
        countProduct = Int((cantidadProducto?.text!)!)!;
        
        if(countProduct > 1){
            countProduct = countProduct - 1;
            cantidadProducto?.text = "\(countProduct)"
        }else {
            return ;
        }
        
        precioProducto?.text = "\(self.calcMontoTotal(cantidad: countProduct, precio: Double(obj_producto.price!)))"
        
    }
    
    
    @IBAction func plusButton(_ sender: Any) {
        countProduct = Int((cantidadProducto?.text!)!)!;
        
        countProduct = countProduct+1;
        
        cantidadProducto?.text = "\(countProduct)";
        
        precioProducto?.text = "\(self.calcMontoTotal(cantidad: countProduct, precio: Double(obj_producto.price!)))"
        
        
    }
    
   
    @IBAction func agregarProducto(_ sender: Any) {
        
        
    }
    
    
    func calcMontoTotal (cantidad:Int, precio:Double) ->Double{
        
        var total = 0.0;
        
        do{
            
            total = precio * Double(cantidad);
            
        }catch {
            
        }
        
        return total.roundTo(places: 2);
        
        
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

